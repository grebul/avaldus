import jsPDF from 'jspdf';

export function convertToPdf(state, todayDate) {
  const pdf = new jsPDF();
  const {
    senderName,
    senderStreetAddress,
    senderPostIndexAndTown,
    senderPhoneNumber,
    senderEmail,
    receiverName,
    receiverAdditionalInfo1,
    receiverAdditionalInfo2,
    content,
    isDigiSigned,
    hasAdditions
  } = state;
  const additions = state.additions.filter((item) => item !== "");
  const pageMargin = 30;
  const pageWidth = 180;
  const rowHeight = 7;
  const splitContent = pdf.splitTextToSize(content, pageWidth - rowHeight);
  const contentHeight = splitContent.length * rowHeight;

  pdf.setFontSize(14);
  // Sender section
  pdf.text(pageWidth, pageMargin, senderName, null, null, 'right');
  pdf.text(pageWidth, pageMargin + rowHeight, senderStreetAddress, null, null, 'right');
  pdf.text(pageWidth, pageMargin + 2 * rowHeight, senderPostIndexAndTown, null, null, 'right');
  pdf.text(pageWidth, pageMargin + 3 * rowHeight, 'tel.: ' + senderPhoneNumber, null, null, 'right');
  pdf.text(pageWidth, pageMargin + 4 * rowHeight, 'e-post: ' + senderEmail, null, null, 'right');

  // Receiver section
  pdf.text(pageMargin, pageMargin + 5 * rowHeight, receiverName);
  pdf.text(pageMargin, pageMargin + 6 * rowHeight, receiverAdditionalInfo1);
  pdf.text(pageMargin, pageMargin + 7 * rowHeight, receiverAdditionalInfo2);

  // Date
  pdf.text(pageWidth, pageMargin + 8 * rowHeight, todayDate, null, null, 'right');

  // Content
  pdf.text(105, pageMargin + 10 * rowHeight, 'AVALDUS', null, null, 'center');
  pdf.text(pageMargin, pageMargin + 12 * rowHeight, splitContent);

  // Closure
  pdf.text(pageMargin, pageMargin + 14 * rowHeight + contentHeight, 'Lugupidamisega');
  pdf.text(pageMargin, pageMargin + 17 * rowHeight + contentHeight, senderName);

  isDigiSigned && pdf.text(pageMargin, pageMargin + 20 * rowHeight + contentHeight,
    '/dokument on digitaalselt allkirjastatud/');

  if (hasAdditions) {
    const verticalPosition = isDigiSigned ? pageMargin + 23 * rowHeight + contentHeight
      : pageMargin + 20 * rowHeight + contentHeight;

    pdf.text(pageMargin, verticalPosition, 'Lisad:');
    additions.forEach((item, index) => {
      pdf.text(pageMargin + rowHeight, verticalPosition + rowHeight * (index + 2),
        index + 1 + '. ' + item);
    });
  }

  pdf.save("download.pdf");
}

export function getTodayDate() {
  const today = new Date();
  let day = today.getDate();
  let month = today.getMonth() + 1;
  const year = today.getFullYear();

  if(day < 10) {
      day = `0${day}`;
  }

  if(month < 10) {
      month = `0${month}`;
  }

  return `${day}.${month}.${year}`;
}
