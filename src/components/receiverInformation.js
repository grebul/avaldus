import React from 'react';
import InputField from './inputField';

const receiverInformation = (props) => (
  <div>
    2. Saaja andmed
    <form className="data-panel__form">
      <InputField
        name="Nimi"
        requiredField={true}
        stateName="receiverName"
        stateValue={props.receiverName}
        placeHolder="Jaan Juurikas"
        handleInput={props.inputHandling} />
      <InputField
        name="Töökoht"
        stateName="receiverAdditionalInfo1"
        stateValue={props.receiverAdditionalInfo1}
        placeHolder="Tallinna Tehnikaülikool"
        handleInput={props.inputHandling} />
      <InputField
        name="Töökoht 2"
        stateName="receiverAdditionalInfo2"
        stateValue={props.receiverAdditionalInfo2}
        placeHolder="Kantselei"
        handleInput={props.inputHandling} />
    </form>
  </div>
);

export default receiverInformation;
