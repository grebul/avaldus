import React from 'react';
import InputField from './inputField';

const senderInformation = (props) => (
  <div>
    1. Sinu andmed
    <form className="data-panel__form">
      <InputField
        name="Nimi"
        requiredField={true}
        stateName="senderName"
        stateValue={props.senderName}
        placeHolder="Mari Maasikas"
        handleInput={props.inputHandling} />
      <InputField
        name="Aadress"
        requiredField={true}
        stateName="senderStreetAddress"
        stateValue={props.senderStreetAddress}
        placeHolder="Rüütli 80-2"
        handleInput={props.inputHandling} />
      <InputField
        name="Aadress 2"
        requiredField={true}
        stateName="senderPostIndexAndTown"
        stateValue={props.senderPostIndexAndTown}
        placeHolder="11111 Tartu"
        handleInput={props.inputHandling} />
      <InputField
        name="Telefoninumber"
        requiredField={true}
        stateName="senderPhoneNumber"
        stateValue={props.senderPhoneNumber}
        placeHolder="55 555 555"
        handleInput={props.inputHandling} />
      <InputField
        name="E-posti aadress"
        requiredField={true}
        stateName="senderEmail"
        stateValue={props.senderEmail}
        placeHolder="mari.maasikas@gmail.com"
        handleInput={props.inputHandling} />
    </form>
  </div>
);

export default senderInformation;
