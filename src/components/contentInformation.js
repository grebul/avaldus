import React from 'react';
import Textarea from 'react-textarea-autosize';
import InputField from './inputField';

const contentInformation = (props) => (
  <div>
    3. Avalduse sisu
    <form className="data-panel__form">
      <label className="data-panel__input-label">
        Avaldus
        <span className="data-panel__input-label--required">*</span> :
        <Textarea
          className="data-panel__input-field"
          name="content"
          type="text"
          value={props.content}
          placeholder="Palun lubada mind..."
          spellCheck="false"
          onChange={props.inputHandling} />
      </label>
      <label className="data-panel__input-label--optional">
        Avaldus saab digiallkirjastatud:
        <input
          className="data-panel__input-field a-panel__input-field--checkbox"
          name="isDigiSigned"
          type="checkbox"
          checked={props.isDigiSigned}
          onChange={props.inputHandling} />
      </label>
      <label className="data-panel__input-label--optional">
        Avaldusele tulevad lisad:
        <input
          className="data-panel__input-field"
          name="hasAdditions"
          type="checkbox"
          checked={props.hasAdditions}
          onChange={props.inputHandling} />
      </label>
      {props.hasAdditions && props.additions.map((item, i) => {
        const name = `Lisa ${i + 1}`;

        return (
          <InputField
            key={i}
            name={name}
            stateName="additions"
            stateValue={item}
            placeHolder="Passi koopia"
            handleInput={(e) => props.inputHandling(e, i)} />
        )})
      }
      {props.hasAdditions &&
        <button type="button" className="data-panel__button--add-row" id="add-row-button" onClick={props.handleAddRowClick}>
          +
        </button>
      }
    </form>
  </div>
);

export default contentInformation;
