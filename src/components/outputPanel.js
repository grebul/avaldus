import React from 'react';

const outputPanel = (props) => {
  const {
    senderName,
    senderStreetAddress,
    senderPostIndexAndTown,
    senderPhoneNumber,
    senderEmail,
    receiverName,
    receiverAdditionalInfo1,
    receiverAdditionalInfo2,
    content,
    hasAdditions,
    isDigiSigned,
    todayDate
  } = props;

  function getAdditionsList() {
    const additions = props.additions;
    const listItems = additions.map((item) => {
      return item && item !== "" && <li key={item}>{item}</li>
    });

    return (
      <div className="additions">
        Lisad:
        <ol>{listItems}</ol>
      </div>
    );
  }

  return (
    <div className="output-panel">
      <div className="page">
        <div className="page__sender">
          <div>{senderName === '' ? 'Sinu andmed' : senderName}</div>
          <div>{senderStreetAddress}</div>
          <div>{senderPostIndexAndTown}</div>
          <div>{senderPhoneNumber !== '' &&  `tel.: ${senderPhoneNumber}`}</div>
          <div>{senderEmail !== '' &&  `e-post: ${senderEmail}`}</div>
        </div>
        <div className="page__receiver">
          <div>{receiverName}</div>
          <div>{receiverAdditionalInfo1}</div>
          <div>{receiverAdditionalInfo2}</div>
        </div>
        <div className="page__date">
          {todayDate}
        </div>
        <div className="page__title">
          AVALDUS
        </div>
        <div className="page__content">
          {content}
        </div>
        <div className="page__courtsey">
          Lugupidamisega
        </div>
        <div className="page__sender-name">
          {senderName}
        </div>
        {isDigiSigned &&
          <div className="page__digi-signature">
            /dokument on digitaalselt allkirjastatud/
          </div>
        }
        <div className="page__additions">
          {hasAdditions && getAdditionsList()}
        </div>
      </div>
    </div>
  );
}

export default outputPanel;
