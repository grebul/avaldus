import React from 'react';

const inputField = (props) => (
  <label className="data-panel__input-label">
    {props.name}
    <span className="data-panel__input-label--required">{props.requiredField && '*'}</span> :
    <input
      className="data-panel__input-field"
      name={props.stateName}
      type="text"
      value={props.stateValue}
      placeholder={props.placeHolder}
      spellCheck="false"
      onChange={props.handleInput} />
  </label>
);

export default inputField;
