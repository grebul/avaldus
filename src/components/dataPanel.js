import React, { useState, useEffect } from 'react';
import posed, { PoseGroup } from 'react-pose';
import SenderInformation from './senderInformation';
import ReceiverInformation from './receiverInformation';
import ContentInformation from './contentInformation';
import { convertToPdf } from '../utils/converter';

const InfoBox = posed.div({
  enter: {
    x: 0,
    opacity: 1,
    delay: 1000,
    transition: {
      default: { duration: 100 }
    }
  },
  exit: {
    x: -100,
    opacity: 0,
    delay: 200,
    transition: { duration: 200 }
  }
});

const dataPanel = (props) => {
  const [state, setState] = useState({
    senderName: '',
    senderStreetAddress: '',
    senderPostIndexAndTown: '',
    senderPhoneNumber: '',
    senderEmail: '',
    receiverName: '',
    receiverAdditionalInfo1: '',
    receiverAdditionalInfo2: '',
    content: '',
    hasAdditions: false,
    additions: [''],
    isDigiSigned: false
  });
  const {todayDate} = props;
  const [inputPanelNumber, setInputPanelNumber] = useState(0);
  const [requiredFieldsFilled, setRequiredFieldsFilled] = useState(false);

  useEffect(() => {
    checkRequiredFieldsFill();
  });

  function checkRequiredFieldsFill() {
    const {
      senderName,
      senderStreetAddress,
      senderPostIndexAndTown,
      senderPhoneNumber,
      senderEmail,
      receiverName,
      content
    } = state;
    const firstPanelUnfilled = inputPanelNumber === 0 &&
      (senderName === '' ||
      senderStreetAddress === '' ||
      senderPostIndexAndTown === '' ||
      senderEmail === '' ||
      senderPhoneNumber === '');
    const secondPanelUnfilled = inputPanelNumber === 1 && receiverName === '';
    const thirdPanelUnfilled = inputPanelNumber === 2 && content === '';

    if (firstPanelUnfilled || secondPanelUnfilled || thirdPanelUnfilled) {
      setRequiredFieldsFilled(false);
    } else {
      setRequiredFieldsFilled(true);
    }
  }

  function handleNextClick() {
    if (requiredFieldsFilled) {
      if (inputPanelNumber < 2) {
        setInputPanelNumber(inputPanelNumber + 1);
      } else {
        convertToPdf(state, todayDate);
      }
    }
  }

  function handleBackClick() {
    if (inputPanelNumber > 0) {
      setInputPanelNumber(inputPanelNumber - 1);
    }
  }

  function addRow() {
    setState(state => {
      const additions = [...state.additions, ''];

      return {
        ...state,
        additions
      };
    });
  }

  function handleInputChange(event, i) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    if (name === 'additions') {
      setState(state => {
        const additions = state.additions.map((item, j) => {
          if (j === i) {
            return value;
          } else {
            return item;
          }
        });

        props.onChange(name, additions);

        return {
          ...state,
          additions
        };
      });
    } else {
      setState(state => ({
       ...state,
        [name]: value
      }));
      props.onChange(name, value);
    }
  }

  function getBackButtonClassNames() {
    return (inputPanelNumber === 0 ? 'data-panel__button data-panel__button--disabled' : 'data-panel__button');
  }

  function getNextButtonClassNames() {
    return (requiredFieldsFilled ? 'data-panel__button' : 'data-panel__button data-panel__button--disabled');
  }

  return (
    <div className="data-panel">
      <div className="data-panel__heading">
        Avaldus 1-2-3
      </div>
      <div className="data-panel__content">
        <PoseGroup>
          {inputPanelNumber === 0 &&
            <InfoBox key="sender">
              <SenderInformation
                senderName={state.senderName}
                senderStreetAddress={state.senderStreetAddress}
                senderPostIndexAndTown={state.senderPostIndexAndTown}
                senderPhoneNumber={state.senderPhoneNumber}
                senderEmail={state.senderEmail}
                inputHandling={handleInputChange} />
            </InfoBox>
          }
          {inputPanelNumber === 1 &&
            <InfoBox key="receiver">
              <ReceiverInformation
                receiverName={state.receiverName}
                receiverAdditionalInfo1={state.receiverAdditionalInfo1}
                receiverAdditionalInfo2={state.receiverAdditionalInfo2}
                inputHandling={handleInputChange} />
            </InfoBox>
          }
          {inputPanelNumber === 2 &&
            <InfoBox key="content">
              <ContentInformation
                content={state.content}
                isDigiSigned={state.isDigiSigned}
                hasAdditions={state.hasAdditions}
                additions={state.additions}
                inputHandling={handleInputChange}
                handleAddRowClick={addRow} />
            </InfoBox>
          }
        </PoseGroup>
        <div className="data-panel__button-wrapper">
          <button className={getBackButtonClassNames()} id="back-button" onClick={handleBackClick}>
            tagasi
          </button>
          <button className={getNextButtonClassNames()} id="forward-button" onClick={handleNextClick}>
            {inputPanelNumber === 2 ? 'faili allalaadimine' : 'edasi'}
          </button>
        </div>
      </div>
    </div>
  );
}

export default dataPanel;
