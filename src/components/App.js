import React, { useState } from 'react';
import DataPanel from './dataPanel';
import OutputPanel from './outputPanel';
import { getTodayDate } from '../utils/converter';

const app = (props) => {
  const [state, setState] = useState({
    senderName: '',
    senderStreetAddress: '',
    senderPostIndexAndTown: '',
    senderPhoneNumber: '',
    senderEmail: '',
    receiverName: 'Saaja andmed',
    receiverAdditionalInfo1: '',
    receiverAdditionalInfo2: '',
    content: 'Avalduse sisu',
    hasAdditions: false,
    additions: [''],
    isDigiSigned: false
  });
  const todayDate = getTodayDate();

  function handleInputChange(name, value) {
    setState(state => ({
     ...state,
      [name]: value
    }));
  }

  return (
    <div className="App">
      <DataPanel onChange={handleInputChange} todayDate={todayDate}/>
      <OutputPanel
        senderName={state.senderName}
        senderStreetAddress={state.senderStreetAddress}
        senderPostIndexAndTown={state.senderPostIndexAndTown}
        senderPhoneNumber={state.senderPhoneNumber}
        senderEmail={state.senderEmail}
        receiverName={state.receiverName}
        receiverAdditionalInfo1={state.receiverAdditionalInfo1}
        receiverAdditionalInfo2={state.receiverAdditionalInfo2}
        content={state.content}
        hasAdditions={state.hasAdditions}
        additions={state.additions}
        isDigiSigned={state.isDigiSigned}
        todayDate={todayDate} />
    </div>
  );
}

export default app;
